# vim:set ft=dockerfile:
FROM golang:1.13-alpine AS builder

RUN apk add --update --no-cache git \
 && CGO_ENABLED=0 installsuffix=cgo go get -ldflags "-extldflags '-static' -s" github.com/deepmap/oapi-codegen/cmd/oapi-codegen

FROM scratch

COPY --from=builder /go/bin/oapi-codegen /oapi-codegen

ENTRYPOINT ["/oapi-codegen"]
